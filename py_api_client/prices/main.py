import time
import cbpro
import redis

public_client = cbpro.PublicClient()

starttime = time.time()

REDIS_HOST = 'redis'
REDIS_PORT = 6379
try:
    red = redis.Redis(
        host=REDIS_HOST,
        port=REDIS_PORT,
        ssl=False)
    print(red)
    red.ping()
    print('Connected')
except Exception as ex:
    print('Error:', ex)
    exit('Failed to connect, terminating.')

while True:
    ETH = public_client.get_product_ticker(product_id='ETH-USD')
    BTC = public_client.get_product_ticker(product_id='BTC-USD')
    SOL = public_client.get_product_ticker(product_id='SOL-USD')
    MATIC = public_client.get_product_ticker(product_id='MATIC-USD')
    AVAX = public_client.get_product_ticker(product_id='AVAX-USD')
    red.set('ETHUSD', float(ETH['price']))
    red.set('BTCUSD', float(BTC['price']))
    red.set('SOLUSD', float(SOL['price']))
    red.set('MATICUSD', float(MATIC['price']))
    red.set('AVAXUSD', float(AVAX['price']))
    time.sleep(1.0 - ((time.time() - starttime) % 1.0))