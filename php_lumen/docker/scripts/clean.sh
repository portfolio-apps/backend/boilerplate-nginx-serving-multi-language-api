#!/bin/bash
echo '----------------------------------------'
tput setaf 6; echo ' Showing Running Containers'; tput sgr 0;
echo '----------------------------------------'
docker ps
echo ''
echo ''

read -p "Would you like to shutdown running containers? " -n 1 -r
echo  ''
if [[ $REPLY =~ ^[Yy]$ ]]
then
  cd ./
  docker-compose down
fi
echo ''
echo ''

echo '----------------------------------------'
tput setaf 6; echo ' Showing Local Images'; tput sgr 0;
echo '----------------------------------------'
docker images
echo ''
echo ''


echo '----------------------------------------'
tput setaf 1; echo ' Preparing to clear all local images...'; tput sgr 0;
echo '----------------------------------------'
docker system prune -a
docker volume prune
echo ''
echo ''


read -p "Would you like to re-build the project? " -n 1 -r
echo  ''
if [[ $REPLY =~ ^[Yy]$ ]]
then
  bash ./docker/scripts/build.sh
fi
echo ''
echo ''
