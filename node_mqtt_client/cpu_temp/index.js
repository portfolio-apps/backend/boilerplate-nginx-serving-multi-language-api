require('dotenv').config()
const mqtt = require('mqtt')
const redis = require('redis');

const redisObj = {
  // url: process.env.REDIS_URL || process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT
  url: process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
  password: process.env.REDIS_PASSWORD,
}
const redisClient = redis.createClient(redisObj);
const mqttClient = mqtt.connect({
  host: process.env.MQTT_HOST,
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: false
});

const topic = 'cpu_temp/#'
console.log('Topic: ', topic)

mqttClient.subscribe(topic)

mqttClient.on('message', function (topic, message) {
    let topic_str = topic.toString()
    let message_str = message.toString()
    // split the topic str into array
    topic_str = topic_str.split('/')
    console.log(message_str)
    redisClient.setex(`node:${topic_str[0]}:${topic_str[1]}`, 3000, JSON.stringify(message_str), redis.print);
})
  